<?php

require_once 'vendor/autoload.php';

use DummyImageGenerator\Config;
use DummyImageGenerator\FS\FileReader;
use DummyImageGenerator\ImageGenerator;
use DummyImageGenerator\TargetPathGenerator;

$options = getopt('t:', ['target-image-base-info']);

$configData = [
    'sourceImagesPath' => './srcImages',
    'targetImagesInfoPath' => '',
    'targetImagesInfo' => '',
    'idExtendPattern' => "%'.012d"
];

Config::initialize($configData);

if (isset($options['t'])) {
    Config::set('targetImagesInfoPath', $options['t']);
} else if (isset($options['target-image-base-info'])) {
    Config::set('targetImagesInfoPath', $options['target-image-base-info']);
}

if (empty(Config::get('targetImagesInfoPath'))) {
    echo "You need to specify the -t or --target-image-base-info argument with the path to the base information.\n\n";
    exit(1);
}

if (!is_readable(Config::get('targetImagesInfoPath'))) {
    echo "The target-image-base-info path is not a readable file.\n\n";
    exit(1);
}

Config::set('targetImagesInfo', FileReader::getContent(Config::get('targetImagesInfoPath')));

$sourceImages = new DummyImageGenerator\SourceImages(Config::get('sourceImagesPath'));
$targetImages = new DummyImageGenerator\TargetImages(Config::get('targetImagesInfo'));
$padLength = strlen((string)$targetImages->count());

while ($imageInfo = $targetImages->next()) {
    if ($targetImages->key() > 0) {
        echo "\033[1A";
    }

    echo sprintf('% ' . $padLength . 's', ($targetImages->key() + 1)) .
        ' of ' .
        $targetImages->count() .
        ' (' .
        floor(($targetImages->key() + 1) * 100 / $targetImages->count()) .
        '%)' .
        "\n";

    $targetPath = TargetPathGenerator::getPathById($imageInfo->fileID) . '_image' . $imageInfo->extension;
    $sourceImage = $sourceImages->get();
    ImageGenerator::generate($imageInfo->width, $imageInfo->height, $sourceImage, './build/' . $targetPath);
}

echo "\n\nDone.\n\n";
