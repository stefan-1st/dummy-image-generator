The purpose of the project is to provide a tool that can be used to create dummy images for a project, eliminating the need to copy large amounts of data over the Internet.

Before using the generator you need to generate the autoload.php with `composer dump-autoload`.

You can run the generator with `composer run generate -- -t <json-file-with-data.json>`
