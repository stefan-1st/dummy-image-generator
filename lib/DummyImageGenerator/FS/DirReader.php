<?php

namespace DummyImageGenerator\FS;

/**
 * Class DirReader
 * @author Stefan Michalsky-Hirschberg <stefan.michalsky@gmail.com>
 * @package DummyImageGenerator\FS
 */
class DirReader
{
    /**
     * Globs the given path and returns the directories content.
     * @param string $path
     * @return array
     * @throws \Exception
     */
    public static function getContent(string $path)
    {
        if (!is_readable($path) || !is_dir($path)) {
            throw new \Exception('The given path is not readable or not a directory.');
        }

        return glob($path . '/*');
    }
}
