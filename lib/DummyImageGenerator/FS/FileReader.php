<?php

namespace DummyImageGenerator\FS;

/**
 * Class FileReader
 * @author Stefan Michalsky-Hirschberg <stefan.michalsky@gmail.com>
 * @package DummyImageGenerator\FS
 */
class FileReader
{
    /**
     * This method will read a file and return its content as a string if possible.
     * @param string $path
     * @return bool|string
     * @throws \Exception
     */
    public static function getContent(string $path)
    {
        if (!is_readable($path) || !is_file($path)) {
            throw new \Exception('The given file is not readable or not a regular file.');
        }

        return file_get_contents($path);
    }
}
