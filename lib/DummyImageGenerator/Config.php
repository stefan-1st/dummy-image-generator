<?php

namespace DummyImageGenerator;

class Config
{
    protected static $config;

    public static function initialize(array $config)
    {
        if (!empty(self::$config)) {
            return;
        }

        static::$config = $config;
    }

    public static function get(string $name)
    {
        if (!isset(static::$config[$name])) {
            throw new \Exception('The requested config value doesn\'t exist.');
        }

        return static::$config[$name];
    }

    public static function set(string $name, $value)
    {
        static::$config[$name] = $value;
    }
}
