<?php

namespace DummyImageGenerator;

/**
 * Class TargetImages
 * @author Stefan Michalsky-Hirschberg <stefan.michalsky@gmail.com>
 * @package DummyImages
 */
class TargetImages
{
    /**
     * This array stores the details of the target images.
     * @var array $images
     */
    private $images;

    /**
     * The iterator is used to keep track which target image is worked on at the moment.
     * @var \ArrayIterator
     */
    private $iterator;

    /**
     * The array index of the element returned by the next() method.
     * @var int
     */
    private $index;

    /**
     * TargetImages constructor.
     * @param string $baseInfo This is a json as a string that holds infos like size of image, target id and extension.
     */
    public function __construct(string $baseInfo)
    {
        $json = json_decode($baseInfo);

        if (!is_array($json)) {
            throw new \Exception('The parameter base info should be a stringified json array.');
        }

        $this->images = $json;
        $this->iterator = new \ArrayIterator($this->images);
    }

    /**
     * Returns either the next target images details or false if no target image is left.
     * @return bool|array
     */
    public function next()
    {
        $current = false;

        if ($this->iterator->valid()) {
            $current = $this->iterator->current();
            $this->index = $this->iterator->key();
            $this->iterator->next();
        }

        return $current;
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->index;
    }

    public function count()
    {
        return $this->iterator->count();
    }
}
