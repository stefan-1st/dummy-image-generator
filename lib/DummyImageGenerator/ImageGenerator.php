<?php

namespace DummyImageGenerator;

/**
 * Class ImageGenerator
 * @author Stefan Michalsky-Hirschberg <stefan.michalsky@gmail.com>
 * @package DummyImages
 */
class ImageGenerator
{
    public static function generate($width, $height, $srcPath, $target)
    {
        if ($width <= 0 || $height <= 0) {
            return;
        }

        $path = dirname($target);

        if (!is_readable($path) && !mkdir($path, 0775, true) && !is_dir($path)) {
			throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
        }

        // prepare the source
        $src = new \Imagick($srcPath);

        $srcWidth = $src->getImageWidth();
        $srcHeight = $src->getImageHeight();
        $srcRatio = $srcWidth / $srcHeight;

        $targetRatio = $width / $height;

        if ($targetRatio > $srcRatio) {
            // target is more landscape, use width, calculate height
            $shrinkRatio = $srcWidth / $width;
            $src->scaleImage($width, intval(ceil($srcHeight / $shrinkRatio)), false);
        } else {
            // target is less landscape, use height, calculate width
            $shrinkRatio = $srcHeight / $height;
            $src->scaleImage(intval(ceil($srcWidth / $shrinkRatio)), $height, false);
        }

        // prepare the new image
        $img = new \Imagick();
        $img->newImage($width, $height, new \ImagickPixel('transparent'));
        $img->addImage($src); // , \Imagick::COMPOSITE_ATOP, 0, 0);

        $img->writeImage($target);
    }
}
