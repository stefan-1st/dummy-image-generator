<?php

namespace DummyImageGenerator;

class TargetPathGenerator
{
    public static function getPathById(string $id)
    {
        $padded = sprintf(Config::get('idExtendPattern'), $id);
        $splitted = str_split($padded, 3);
        $path = implode('/', $splitted);

        return $path;
    }
}