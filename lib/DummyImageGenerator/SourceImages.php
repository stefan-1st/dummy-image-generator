<?php

namespace DummyImageGenerator;
use DummyImageGenerator\FS\DirReader;

/**
 * The class SourceImages is used to make source images available. It will load all source images and it will grant access
 * to them.
 * @author Stefan Michalsky-Hirschberg <stefan.michalsky@gmail.com>
 * @package DummyImages
 */
class SourceImages
{
    /**
     * This member is used to store the infos of the loaded source images.
     * @var array $store
     */
    private $store;

    /**
     * The absolute system path to the directory that holds the images that are used as source.
     * @var string $sourcePath
     */
    protected $sourcePath;

    /**
     * SourceImages constructor.
     * @param string $sourcePath
     */
    public function __construct(string $sourcePath)
    {
        $this->sourcePath = $sourcePath;
        $this->store = [];

        $this->initialize();
    }

    /**
     * Take the given directory and load all images from it.
     * @return int
     */
    public function initialize()
    {
        $this->store = DirReader::getContent(Config::get('sourceImagesPath'));
    }

    public function get()
    {
        return $this->store[rand(0, count($this->store) - 1)];
    }
}
